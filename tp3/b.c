
#include <stdio.h> 

int main(void) 
{ 
   float nombre=0; 


   // ici || est un OU logique (pour un ET il faudrait &&)
   while ((nombre<50) || (nombre>60)) { 
       printf("Entrer un nombre : "); 
       scanf("%f",&nombre); 
       if (nombre<50) 
           puts("Saisissez un nombre plus grand !"); 
       else if (nombre>60) 
           puts("Saisissez un nombre plus petit !"); 
       else 
           puts("Bravo"); 
   } 
     
return 0; 
}
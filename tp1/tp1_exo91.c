#include <stdio.h>


int main(void){


char code;

printf("Entrer le code = ");
scanf("%c",&code);

switch (code) {
	
	case 'V':
		printf("Catégorie Masters de 1975 ou avant.\n");
		break;

	case 'S':
		printf("Catégorie Seniors de 1993 à 1995.\n");
		break;

	case 'E':
		printf("Catégorie Espoirs de 1993 à 1995.\n");
		break;
	
	case 'J':
		printf("Catégorie Juniors de 1996 à 1997.\n");
		break;

	case 'C':
		printf("Catégorie Cadets de 1998 à 1999.\n");
		break;

	case 'M':
		printf("Catégorie Minimes de 2000 à 2001.\n");
		break;

	case 'B':
		printf("Catégorie Benjamins de 2002 à 2003.\n");
		break;


	case 'P':
		printf("Catégorie Poussins de 2004 à 2005.\n");
		break;

	default:
		printf("Catégorie ... tu t'es trompés de code !\n");
}

		
return 0;
}
